---
title: "Email"
description: "Servicio gratuito de email forwarding con las cuentas de Mastodon de tu.social."
date: 2023-05-28T20:35:50+02:00
lastmod: 2023-05-28T20:35:50+02:00
draft: false
images: []
toc: true
---

Al registrarse en la instancia de Mastodon de tu.social, se creará automáticamente una cuenta de correo con el mismo nombre, que reenviará todos los correos a la dirección asociada a esa cuenta.

Por ejemplo al crear `oleg@tu.social`, automáticamente los correos enviados a `oleg@tu.social` llegarán a la bandeja de entrada del correo con el que me he registrado.
